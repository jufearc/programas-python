#!/usr/bin/python
# -*- coding: utf-8 -*-

variable1 = 5

variable2 = 5.0

variable3 = 5


print "5 es igual a 5.0 es "    + str(variable1 is variable2)
#las variables son similares, pero no identicas, variable1 almacena un entero. variable2  almacena un flotante, por lo que el resultado de la operacion es falso

print variable1 is not variable2
#es cierto, ya que son variables con valor diferentes

print variable3 is variable1
# cierto, ya que ambas variables tienen como valor el numero entero 5
