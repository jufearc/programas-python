#!/usr/bin/python
# -*- coding: utf-8 -*-

libro = "El nombre del viento"

numero_de_paginas = "más de 800", "aproximadamente"

dias = 4,

autor = "Patrick Rufuss"

resena = "Una pieza maestra, llena de suspenso, emoción, fantasía y sorpresas, con una capacidad para transportarte hasta otra dimension fascinante, llena de amor, ciencia, magia y diversion"

escrito_por = "Reseña hecha por Juan Araujo"


cronica = "Asesino de reyes"

print libro
#imprime el nombre del libro
print numero_de_paginas
#el numero aproximado de paginas
print dias
#dias de lectura 
print autor
#autor del libro
print resena
#comentario general
print escrito_por
#quien escribio la reseña
print cronica
#si pertenece a un colección de libros
