#!/usr/bin/python
# -*- coding:utf-8 -*-

lista = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]

print lista[2:17]
#Muestra "[3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]"
print lista[2:15:3]
# imprime [3,6,9,12,15]
print lista[2:15:2]
#imprime [3,5,7,9,11,13,15]
