#!/usr/bin/python
# *-* coding:utf-8 *-*

diccionario = {"animal": "gato", "cosa": "piedra", "planta": "lechuga"}


print diccionario
#imprime "animal": "gato", hasta : "lechuga

print diccionario["planta"]

#imprime lechuga

diccionario["planta"] = "coliflor"

print diccionario

print diccionario["planta"]
#ahora imprime coliflor
