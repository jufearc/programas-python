#!/usr/bin/python
# --- coding: utf-8 *-*

lista = ["silla","mesa","armario","taburete"]

print("silla" in lista)

#Muestra "True", porque "silla" esta en la matriz lista

print("sofa" in lista)
#Muestra "False", porque Sofa no esta en lista
idiomas_antiguos = ["Frances", "Ingles", "Aleman"]

idioma_moderno = "Esperanto"

raiz_ingles = idiomas_antiguos.index("Ingles")

Esperanto = 7

Eleccion = Esperanto > raiz_ingles

print(Eleccion)

print(lista[2])

print(raiz_ingles)

print("mesa" not in lista)

#muestra "False" porque "mesa" si esta en la lista
