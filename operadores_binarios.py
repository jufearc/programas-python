#!/usr/bin/python
# -+- coding: utf-8 +-+

resultado = 3 == 2

print resultado
# 3 no es igual  a dos, o dos no es igual a 3. Por lo que es falso

print True and True
#algo que es "verdadero" y "verdadero" es cierto, o en otras palabras.. verdadero

print True and resultado
#algo que es cierto y falso, es falso


print not True
#"no verdadero" es falso

print False or resultado
#la variable que es "falsa" o "falsa", es falsa

print True or resultado
#si alguna de las variables es verdadera, el resultado es verdadera 
