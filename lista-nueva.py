#!/usr/bin/python
# *_* coding: utf-8 *_*

lista = ["primero", "segundo", "tercero", "cuarto", "quinto"]

#0 == "primero"
#1 == "segundo"
#2 == "tercero"
print lista[2]
#imprime "tercero"


lista[2] = "tri"

print lista[2]
#en esta ocasion, como se cambio el valor 3 si contamos de 0 a 2, muestra tri
